#!/usr/bin/python
from random import shuffle, randrange, random

MAX_QS = 20
NUM_PACKETS = 19

CATEGORIES = [
    "Lit - American", "Lit - British", "Lit - European", "Lit - World/Misc",
    "Hist - US", "Hist - Euro (1)", "Hist - Euro (2)", "Hist - World",
    "Sci - Phys/Astro", "Sci - Chem/Earth/Other", "Sci - Bio", "Sci - Math/CS",
    "FA - Visual", "FA - Auditory", "FA - Other",
    "Religion",
    "Philosophy",
    "Social Science",
    "Myth/Geo/MW/OA - (1)", "Myth/Geo/MW/OA - (2)",
]

T_DISTRIBUTION = {
    cat: NUM_PACKETS for cat in CATEGORIES
}

B_DISTRIBUTION = {
    cat: NUM_PACKETS for cat in CATEGORIES
}

PER_PACKET_MIN = {"Lit":            4,
                  "Hist":           4,
                  "Sci":            4,
                  "FA":             3,
                  "Religion":       1,
                  "Philosophy":     1,
                  "Social Science": 1,
                  "Myth/Geo/MW/OA": 2}

PER_PACKET_MAX = {"Lit":            4,
                  "Hist":           4,
                  "Sci":            4,
                  "FA":             3,
                  "Religion":       1,
                  "Philosophy":     1,
                  "Social Science": 1,
                  "Myth/Geo/MW/OA": 2}


class Packet:
    def __init__(self):
        self.tossups = []
        self.bonuses = []

    def __repr__(self):
        return ", ".join(self.tossups) + " --- " + ", ".join(self.bonuses)

    def count_cat(self, cat):
        return (len([True for t in self.tossups if cat == cat_from_subcat(t)]),
                len([True for b in self.bonuses if cat == cat_from_subcat(b)]))


def cat_from_subcat(subcat):
    if '-' in subcat:
        return subcat[:subcat.find(' ')]
    else:
        return subcat


def is_shuffled(questions):
    for i in range(len(questions) - 1):
        if cat_from_subcat(questions[i]) == cat_from_subcat(questions[i + 1]):
            return False
    return True


def shuffle_packet(packet):
    packet.tossups = shuffle_questions(packet.tossups)
    packet.bonuses = shuffle_questions(packet.bonuses)

    while True:
        # shuffle packet until no duplicates are next to each other
        while (not is_shuffled(packet.tossups)):
            packet.tossups = shuffle_questions(packet.tossups)
        while (not is_shuffled(packet.bonuses)):
            packet.bonuses = shuffle_questions(packet.bonuses)

        for i in range(MAX_QS):
            if cat_from_subcat(packet.tossups[i]) == cat_from_subcat(packet.bonuses[i]):
                if random() < 0.5:
                    packet.tossups = shuffle_questions(packet.tossups)
                else:
                    packet.bonuses = shuffle_questions(packet.bonuses)
                break
        else:
            break


def shuffle_questions(questions):
    shuffle(questions)

    cats_in_questions = dict()

    for q in questions:
        c = cat_from_subcat(q)
        if c not in cats_in_questions:
            cats_in_questions[c] = tuple((q,))
        else:
            cats_in_questions[c] = tuple([*cats_in_questions[c], q])

    slots = [None] * len(questions)

    def place_in_range(value, begin, end):
        index = randrange(begin, end)
        orig_index = index

        # walk through looking for an open slot ("linear probing")
        while (slots[index] != None):
            index = (index + 1) % (end - begin) + begin
            if index == orig_index:
                return False

        slots[index] = value
        return True

    for cat in cats_in_questions:
        # evenly distribute by interval:
        # e.g. if 4 questions in category, one each in ranges 0-5, 6-10, 11-15, 16-20
        num_cat = len(cats_in_questions[cat])
        interval = MAX_QS // num_cat

        for i in range(num_cat):
            value = cats_in_questions[cat][i]
            if not place_in_range(value, i * interval, (i + 1) * interval):
                # if the designated interval is full, just place anywhere (should be rare case)
                place_in_range(value, 0, MAX_QS)

    return slots


def generate_packets():
    """
    Generates NUM_PACKETS distributions
    """
    packets = [Packet() for i in range(NUM_PACKETS)]

    # Generating packets takes two stages of randomization: in this first step, categories are added
    # to distributions randomly - inter-packet pseudo-randomness. The second step gives intra-packet
    # psuedo-randomness by distributing within packets

    # Do tossups first
    for subcat in T_DISTRIBUTION:
        # number of packets to give this subcat
        count = T_DISTRIBUTION[subcat]
        cat = cat_from_subcat(subcat)

        # number of packets added to
        added = 0
        p = 0

        while added < count:
            if packets[p].count_cat(cat)[0] < PER_PACKET_MAX[cat]:
                packets[p].tossups.append(subcat)
                added += 1

            else:
                count += 1

            p = (p + 1) % NUM_PACKETS

            if count > 100:
                raise Exception()

        shuffle(packets)

        # sort by length so that emptier (1 less) packets are added to first
        packets.sort(key=lambda x: len(x.tossups))

    # Now insert bonuses
    for subcat in B_DISTRIBUTION:
        # number of packets to give this subcat
        count = B_DISTRIBUTION[subcat]
        cat = cat_from_subcat(subcat)

        # number of packets added to
        added = 0
        p = 0

        while added < count:
            if packets[p].count_cat(cat)[1] < PER_PACKET_MAX[cat]:
                packets[p].bonuses.append(subcat)
                added += 1

            p = (p + 1) % NUM_PACKETS

        shuffle(packets)

        # sort by length so that emptier (1 less) packets are added to first
        packets.sort(key=lambda x: len(x.bonuses))

    # nasty but necessary? hack: because of odd numbers, one packet is 1 short and one is 1 long
    # only applies when odd number of packets, i think
    # packets[0].bonuses.append(packets[-1].bonuses[-1])
    # packets[-1].bonuses = packets[-1].bonuses[:-1]

    return packets


packets = generate_packets()

for packet in packets:
    shuffle_packet(packet)

# packets which are anomalous because of odd numbers will be at the front before this step;
# consign them to the oblivion of emergency / tiebreakers
packets.reverse()

"""
# Printing for pasting into Google Sheets:
for i in range(MAX_QS):
    for j in range(NUM_PACKETS):
        print(packets[j].tossups[i] + "," + packets[j].bonuses[i] + ",,", end="")
    print()
"""

# Printing for pasting into question docs
for j in range(NUM_PACKETS):
    print(f"Packet {j + 1}")
    print("---------")

    print("Tossups")
    print()
    for i in range(MAX_QS):
        print(f"{i + 1}. {packets[j].tossups[i]}")
        print()

    print("Bonuses")
    print()
    for i in range(MAX_QS):
        print(f"{i + 1}. {packets[j].bonuses[i]}")
        print()
