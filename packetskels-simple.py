import random

quad_cats = {'L': 4, 'H': 4, 'S': 4}

cats = {'FA': 3, 'RMPT': 3, 'G': 1, 'T': 1}

def place_in_range(slots, letter, start, stop):
  index = random.randrange(start, stop)

  while slots[index] != '':
    index = random.randrange(start, stop)

  slots[index] = letter

def place_in_quadrants(slots, letter, num_of_quads):
  quads_to_place = [0,1,2,3]
  random.shuffle(quads_to_place)

  quads_to_place = quads_to_place[:num_of_quads]

  for quad in quads_to_place:
    place_in_range(slots, letter, quad * 5, (quad + 1) * 5)

def generate_order():
  slots = [''] * 20
  for cat in quad_cats.keys():
    place_in_quadrants(slots, cat, quad_cats[cat])

  for cat in cats.keys():
    for num in range(cats[cat]):
      place_in_range(slots, cat, 0, 20)

  return slots

def verify_order(slots):
  for i in range(len(slots) - 1):
    if slots[i + 1] == slots[i]:
      return False
  return slots

skeleton = False
while not skeleton:
  skeleton = verify_order(generate_order())

print(skeleton)
