import random
from z3 import *
from tabulate import tabulate

NUM_QS_IN_PACKET = 20
TOSSUP_NAME = "tossup"
BONUS_NAME = " bonus"

CATEGORIES = {
    "Lit" : 4,
    "Hist" : 4,
    "Sci" : 4,
    "FA" : 3,
    "Rel" : 1,
    "Myth" : 1,
    "Phil": 1,
    "SS": 1,
    "PC": 1
}

s = Solver()
set_option('smt.arith.random_initial_value', True)
set_option('smt.random_seed', random.randint(0, 2 ** 8))

bool_vars = dict()
# goes from (question type, question number, category) to Bool type

for question_type in (TOSSUP_NAME, BONUS_NAME):
    for question_number in range(1, NUM_QS_IN_PACKET + 1):
        for category in CATEGORIES:
            q_num_str = (" " if question_number <= 9 else "") + str(question_number)
            var = Bool(f"{question_type} {q_num_str}: {category}")
            bool_vars[(question_type, question_number, category)] = var

# generate these kinds of constraints:
# 1. one category per (question type, question number)
# 2. correct numbers of (sub)categories
# 2.5. maximum numbers within halves, quarters
# 3. intra-type adjacency constraints on categories
# 4. inter-type adjacency constraints

all_cats = list(CATEGORIES.keys())

# 1
for question_type in (TOSSUP_NAME, BONUS_NAME):
    for question_number in range(1, NUM_QS_IN_PACKET + 1):
        for i, cat_1 in enumerate(all_cats):
            for j in range(i + 1, len(all_cats)):
                cat_2 = all_cats[j]

                cat_1_var = bool_vars[(question_type, question_number, cat_1)]
                cat_2_var = bool_vars[(question_type, question_number, cat_2)]

                s.add(Not(And([cat_1_var, cat_2_var])))

# 2
for question_type in (TOSSUP_NAME, BONUS_NAME):
    for cat, cat_occurrences in CATEGORIES.items():
        these_qs = [bool_vars[(question_type, question_number, cat)] for question_number in range(1, NUM_QS_IN_PACKET + 1)]
        constraint = PbEq(list(zip(these_qs, [1 for i in range(NUM_QS_IN_PACKET)])), cat_occurrences)
        s.add(constraint)

# 2.5 - for 4-question categories, 1 in each quarter
for question_type in (TOSSUP_NAME, BONUS_NAME):
    for cat, cat_occurrences in CATEGORIES.items():
        if cat_occurrences != 4:
            continue

        for start_quarter_packet, end_quarter_packet in ((1, 5), (6, 10), (11, 15), (16, 20)):
            quarter = [bool_vars[(question_type, question_number, cat)] for question_number in range(start_quarter_packet, end_quarter_packet + 1)]
            s.add(PbEq(list(zip(quarter, [1 for i in range(5)])), 1))

# 3 - no adjacent questions in the same category
for question_type in (TOSSUP_NAME, BONUS_NAME):
    for cat, cat_occurrences in CATEGORIES.items():
        for i in range(1, NUM_QS_IN_PACKET):
            first = bool_vars[(question_type, i, cat)]
            second = bool_vars[(question_type, i + 1, cat)]

            s.add(Not(And([first, second])))

# 4 - no corresponding questions in the same category
for cat, cat_occurrences in CATEGORIES.items():
    for i in range(1, NUM_QS_IN_PACKET + 1):
        tossup = bool_vars[(TOSSUP_NAME, i, cat)]
        bonus = bool_vars[(BONUS_NAME, i, cat)]

        s.add(Not(And([tossup, bonus])))

def print_out_model(model):
    lines = []
    for decl in model.decls():
        if model[decl]:
            lines.append(str(decl))
    lines.sort()

    table = [[lines[NUM_QS_IN_PACKET + i], lines[i]] for i in range(NUM_QS_IN_PACKET)]
    print(tabulate(table))

s.check()
print_out_model(s.model())
